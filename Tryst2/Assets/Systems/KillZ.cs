﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZ : MonoBehaviour{

    public LevelManager levelManager;

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player1") {
            levelManager.LoadLastLevelPlayed();
        }
    }
}
