﻿using UnityEngine;

public class Key : MonoBehaviour
{
	float speed = 10;
	bool isHeld;
	bool isUsed;
	bool isFinished;
	Transform playerTransform;
	Keychain kChain;
	GameObject lockTransform;
	float keyOffset = 1.25f;
	Vector3 keyFinalOffset = new Vector3(-0.5f, 0, 0);
	Vector3 keyFinalEulerOffset = new Vector3(0, 30, 90);
	Animator anim;

	private void Start()
	{
		anim = GetComponent<Animator>();
	}

	private void Update()
	{
		if (isFinished)
		{
			return;
		}

		if (isUsed)
		{
			transform.parent = lockTransform.transform;
			Vector3 targetPos = lockTransform.transform.position;
			targetPos -= lockTransform.transform.TransformDirection(Vector3.right)*keyOffset;
			print(targetPos);
			//targetPos += keyOffset;
			transform.position = Vector3.Lerp(transform.position, targetPos, speed * Time.deltaTime);
			transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles, lockTransform.transform.localEulerAngles, speed * Time.deltaTime);
			if(Vector3.Distance(transform.position, targetPos) <= 0.1f)
			{
				anim.SetTrigger("keyUsed");
			}
			return;
		}

		if (isHeld)
		{
			Vector3 targetPos = playerTransform.transform.position;
			targetPos.y += 3;
			transform.position = Vector3.Lerp(transform.position, targetPos, speed*Time.deltaTime);
		}
	}

	public void OpenLock()
	{
		lockTransform.GetComponent<Animator>().SetTrigger("unlock");		
	}

	public void ParentToLock()
	{
		//transform.parent = lockTransform.transform;
		isFinished = true;
		//transform.localPosition = keyFinalOffset;
		//transform.eulerAngles = keyFinalEulerOffset;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (isHeld)
		{
			if(other.gameObject.tag == "Lock" && !isUsed)
			{
				lockTransform = other.gameObject;
				isUsed = true;
				if(kChain != null){
					kChain.hasPhysicalKey = false;
				}
			}
			return;
		}
		if(other.gameObject.tag == "Player1" || other.gameObject.tag == "Player2")
		{
			kChain = other.gameObject.GetComponent<Keychain>();
			if(!kChain.hasPhysicalKey){
				kChain.hasPhysicalKey = true;
				playerTransform = other.transform;
				isHeld = true;
			} else {
				kChain = null;
			}
			
		}
	}
}
