﻿using UnityEngine;
using DG.Tweening;

public class Lock : MonoBehaviour
{
	[SerializeField] private bool isOpen = false; 
	[SerializeField] private KEY_COLOUR lockColour = KEY_COLOUR.WHITE;
	[SerializeField] private Transform lockTransform;
	private GameObject currentPlayer;

	private void Update()
	{
		if (isOpen)
		{
			lockTransform.DOScaleY(0.25f, 0.5f);
		} else
		{
			lockTransform.DOScaleY(1, 0.5f);
		}
	}

	private void OnTriggerStay(Collider other)
	{
		if(other.tag == "Player1" || other.tag == "Player2")
		{
			if (other.GetComponent<Keychain>().currentKeyColour == lockColour)
			{
				isOpen = true;
				currentPlayer = other.gameObject;
			}
		}
	}
	private void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player1" || other.tag == "Player2")
		{
			if (currentPlayer != null)
			{
				if (other.tag == currentPlayer.tag)
				{
					isOpen = false;
					currentPlayer = null;
				}
			}
		}
	}
}
