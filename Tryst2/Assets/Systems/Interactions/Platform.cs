﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
	[SerializeField] private Transform platformEndTransform;
	[SerializeField] private GameObject solidStructure;
	[SerializeField] private bool isActive;
	[SerializeField] private bool isAutomatic;
	[SerializeField] private bool isSolid;
	[SerializeField] private float extraSolidDistance;
	private bool atEnd;
	public Vector3 externalMoveSpeed;
	private Vector3 lastPos;
	private Vector3 currentPos;
	private Vector3 startPos;
	private Vector3 endPos;
	private float journeyLength;
	private float startTime;
	[SerializeField] private float movesSpeed = 5f;
	[SerializeField] private float waitTime = 2.5f;
	private float timer = 0f;

	private void Start()
	{
		currentPos = transform.position;
		startPos = transform.position;
		endPos = platformEndTransform.position;
		journeyLength = Vector3.Distance(startPos, endPos);
		startTime = Time.time;
		if (isSolid)
		{
			if(extraSolidDistance != 0){
				solidStructure.transform.localScale = new Vector3(1, extraSolidDistance, 1);
				solidStructure.transform.position = new Vector3(transform.position.x, transform.position.y - extraSolidDistance / 2 - 0.5f, transform.position.z);
			} else {
				solidStructure.transform.localScale = new Vector3(1, journeyLength, 1);
				solidStructure.transform.position = new Vector3(transform.position.x, transform.position.y - journeyLength / 2 - 0.5f, transform.position.z);
			}
			
		} else
		{
			solidStructure.GetComponent<MeshRenderer>().enabled = false;
		}
	}

	private void FixedUpdate()
	{
		externalMoveSpeed = transform.position - lastPos;
		if (isActive)
		{
			if (isAutomatic)
			{
				if (!atEnd)
				{
					Move(endPos);
				} else
				{
					Move(startPos);
				}
			} else
			{
				Move(endPos);
			}	
		} else
		{
			Move(startPos);
		}
		lastPos = transform.position;
	}

	private void Move(Vector3 targetPos)
	{
		float distCovered = (Time.time - startTime) * movesSpeed;

		float fractionOfJourney = distCovered / journeyLength;

		GetComponent<Rigidbody>().MovePosition(Vector3.Lerp(currentPos, targetPos, fractionOfJourney));
		if (Vector3.Distance(transform.position, targetPos) == 0)
		{
			if (isAutomatic)
			{
				timer += Time.deltaTime;
				if (timer >= waitTime)
				{
					timer = 0f;
					startTime = Time.time;
					currentPos = transform.position;

					if (atEnd)
					{
						atEnd = false;
					}
					else
					{
						atEnd = true;
					}

					//Activate();
				}
			}			
		}
	}

	private void Activate()
	{
		if (isActive)
		{
			isActive = false;
			startTime = Time.time;
			journeyLength = Vector3.Distance(transform.position, startPos);
			currentPos = transform.position;
		} else
		{
			isActive = true;
			startTime = Time.time;
			journeyLength = Vector3.Distance(transform.position, endPos);
			currentPos = transform.position;
		}
	}
}
