﻿using UnityEngine;

public class Door : MonoBehaviour
{
	public bool isOpen = false;
	private Animation animation = null;
	[SerializeField] private GameObject doorGO;
	[SerializeField] private AnimationClip[] clip = new AnimationClip[2];
	private float playSpeed = 2f;
	private BoxCollider box;

	private void Start()
	{
		animation = doorGO.GetComponent<Animation>();
		box = doorGO.GetComponent<BoxCollider>();

		if (isOpen)
		{
			float time = clip[0].name.Length;
			animation[clip[0].name].time = time;
			animation.Play(clip[0].name);
			box.isTrigger = true;
		}
	}

	public void Activate ()
	{
		if (isOpen)
		{
			isOpen = false;
			animation.CrossFade(clip[1].name, 0.25f, PlayMode.StopAll);
			box.isTrigger = false;
		} else
		{
			isOpen = true;
			animation.CrossFade(clip[0].name, 0.25f, PlayMode.StopAll);
			box.isTrigger = true;
		}
	}
}
