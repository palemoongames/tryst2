﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum GRAVITY_WELL_MODE
{
	Reverseable,
	Activateable,
	LightShaft,
}

public class GravityWell : MonoBehaviour
{
	[SerializeField] GRAVITY_WELL_MODE mode = GRAVITY_WELL_MODE.Reverseable;
 	[SerializeField] private float wellSpeed = 1;
	[SerializeField] private Vector3 wellSize;
	[SerializeField] private GameObject wellParticles;
	[SerializeField] private GameObject wellParticlesReversed;
	[SerializeField] private GameObject wellMesh;
	[SerializeField] private bool isReversed;
	[SerializeField] private bool isActive;

	private Vector3 centerPoint;

	private Vector3 tmpDebugVector;

	private void Start()
	{
		switch (mode)
		{
			case GRAVITY_WELL_MODE.Reverseable:
				SetFlowDirection();
				break;
			case GRAVITY_WELL_MODE.Activateable:
				SetActive(wellParticles,isActive);
				SetActive(wellParticlesReversed, false);
				break;
			case GRAVITY_WELL_MODE.LightShaft:
				wellParticlesReversed.GetComponent<ParticleSystem>().Stop();
				wellParticlesReversed.GetComponent<ParticleSystemRenderer>().enabled = false;
				if (isActive)
				{
					wellParticles.GetComponent<ParticleSystem>().Play();
					wellParticles.GetComponent<ParticleSystemRenderer>().enabled = true;
				}
				else
				{
					wellParticles.GetComponent<ParticleSystem>().Pause();
					wellParticles.GetComponent<ParticleSystemRenderer>().enabled = false;
				}
				break;
		}
	}

	public void InitGravityWell()
	{
		//centerPoint = transform.position + wellSize / 2;
		centerPoint = transform.position;
		wellMesh.transform.position = centerPoint;

		Vector3 adjustedScale = new Vector3(wellSize.x / 2f, wellSize.y / 2, wellSize.z);
		wellMesh.transform.localScale = adjustedScale;

		Vector3 offsetCenterPos = new Vector3(0, 0, -wellSize.z / 2);
		wellParticles.transform.localPosition = offsetCenterPos;

		offsetCenterPos = new Vector3(0, 0, wellSize.z/2);
		wellParticlesReversed.transform.localPosition = offsetCenterPos;

		var particles = wellParticles.GetComponent<ParticleSystem>().main;
		particles.startLifetime = wellSize.z / 2;

		particles = wellParticlesReversed.GetComponent<ParticleSystem>().main;
		particles.startLifetime = wellSize.z / 2;

		var psShape = wellParticles.GetComponent<ParticleSystem>().shape;
		psShape.scale = new Vector3(wellSize.x, wellSize.y, 0);
		psShape = wellParticlesReversed.GetComponent<ParticleSystem>().shape;
		psShape.scale = new Vector3(wellSize.x, wellSize.y, 0);
	}

	private void SetFlowDirection()
	{
		if (!isReversed)
		{
			SetActive(wellParticles, true);
			SetActive(wellParticlesReversed, false);
		}
		else
		{
			SetActive(wellParticles, false);
			SetActive(wellParticlesReversed, true);
		}
	}
	private void SetActive(GameObject go, bool activate)
	{
		if (activate)
		{
			go.GetComponent<ParticleSystem>().Play();
			go.GetComponent<ParticleSystemRenderer>().enabled = true;
		}
		else
		{
			go.GetComponent<ParticleSystem>().Pause();
			go.GetComponent<ParticleSystemRenderer>().enabled = false;
		}
	}

	public void Lift(GameObject GO)
	{
		if (isActive)
		{
			Vector3 dir = wellParticles.transform.TransformDirection(Vector3.forward);
			if (isReversed) {
				dir *= -1;
			}

			Vector3 mid = wellParticlesReversed.transform.position;
			if (isReversed)
			{
				mid = wellParticles.transform.position;
			}
			mid -= GO.transform.position;
			
			dir = (dir + mid).normalized;
			
			GO.GetComponent<Rigidbody>().useGravity = false;
			GO.GetComponent<Rigidbody>().isKinematic = true;
			
			//GO.transform.position += dir * Time.deltaTime * 2;
			if(mode == GRAVITY_WELL_MODE.LightShaft)
			{
				GO.GetComponent<CharacterController>().Move(dir * wellSpeed * Time.deltaTime);
				//GO.GetComponent<Rigidbody>().AddForce(,ForceMode.VelocityChange);
			} else
			{
				GO.GetComponent<CharacterController>().Move(dir * wellSpeed * Time.deltaTime);
				//GO.GetComponent<Rigidbody>().velocity = dir * Time.deltaTime * wellSpeed;
			}
			
		}
	}

	public void Drop(GameObject GO)
	{
		GO.GetComponent<Rigidbody>().useGravity = true;

		GO.GetComponent<Rigidbody>().isKinematic = false;
	}

	public void Activate ()
	{
		switch (mode)
		{
			case GRAVITY_WELL_MODE.Reverseable:
				if (isReversed)
				{
					isReversed = false;
				}
				else
				{
					isReversed = true;
				}
				SetFlowDirection();
				break;
			case GRAVITY_WELL_MODE.Activateable:
				if (isActive)
				{
					isActive = false;
				}
				else
				{
					isActive = true;
				}
				SetActive(wellParticles, isActive);
				break;
			case GRAVITY_WELL_MODE.LightShaft:
				if (isActive)
				{
					isActive = false;
					wellParticles.GetComponent<ParticleSystem>().Play();
				}
				else
				{
					isActive = true;
					wellParticles.GetComponent<ParticleSystem>().Stop();	
				}
				break;
		}
		
	}
}