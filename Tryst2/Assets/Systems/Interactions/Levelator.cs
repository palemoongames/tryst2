﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Levelator : MonoBehaviour
{
	private bool player1;
	private bool player2;
	public bool isComplete;

	public void Update()
	{
		if(player1 && player2)
		{
			isComplete = true;
		} else
		{
			isComplete = false;
		}
	}

	public void OnTriggerStay(Collider other)
	{
		if (other.tag == "Player1")
		{
			player1 = true;
		}
		if (other.tag == "Player2")
		{
			player2 = true;
		}
	}

	public void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player1")
		{
			player1 = false;
		}
		if (other.tag == "Player2")
		{
			player2 = false;
		}
	}
}
