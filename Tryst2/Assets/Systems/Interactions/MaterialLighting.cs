﻿using UnityEngine;
using DG.Tweening;

public class MaterialLighting : MonoBehaviour
{
	[SerializeField] private Renderer renderer = null;
	[SerializeField] private Light light = null;
	[SerializeField] private Transform lightShaft = null;
	public GameObject attachedLighting = null;
	[SerializeField] [ColorUsage(true,true)] private  Color hdrColour = new Vector4();
	public bool isActive = false;
	[SerializeField] private bool onlyOnce = false;
	[SerializeField] private float transitionTime = 1f;
	[SerializeField] private float[] yPos = new float[2];

	private bool hasInitialized = false;
	private bool waiting = false;
	private float timer = 0;

	private void Start()
	{
		DOTween.Init();
		DOTween.SetTweensCapacity(1250, 50);

		if (isActive)
		{
			renderer.material.DOColor(hdrColour, "Color_54C200CD", 0);
			if (light != null)
			{
				light.enabled = true;
			}
			if (lightShaft != null)
			{
				lightShaft.DOScaleY(yPos[0], 0);
				var emission = GetComponentInChildren<ParticleSystem>().emission;
				emission.enabled = true;
			}
		
		}
		else
		{
			renderer.material.DOColor(Color.black, "Color_54C200CD", 0);
			if (light != null)
			{
				light.enabled = false;
			}
			if (lightShaft != null)
			{
				lightShaft.DOScaleY(yPos[1], 0);
				var emission = GetComponentInChildren<ParticleSystem>().emission;
				emission.enabled = false;
			}
		}
	}

	public void Activate ()
	{
		hasInitialized = true;
		waiting = true;
		timer = transitionTime/5;
		if (isActive)
		{
			if (!onlyOnce)
			{
				isActive = false;
			}
		} else
		{
			isActive = true;
		}
	}

	void Update()
	{
		if (!hasInitialized)
		{
			return;
		}
		timer -= Time.deltaTime;
		if (timer < 0)
		{
			timer = 0;
		}
		if (isActive)
		{
			renderer.material.DOColor(hdrColour, "Color_54C200CD", transitionTime);
			if (light != null)
			{
				light.enabled = true;
			}
			if (lightShaft != null)
			{
				lightShaft.DOScaleY(yPos[0], transitionTime);
				var emission = GetComponentInChildren<ParticleSystem>().emission;
				emission.enabled = true;
			}
			if (attachedLighting != null)
			{

				//attachedLighting.SendMessage("Activate");
				if (waiting && timer <= 0)
				{
					attachedLighting.SendMessage("Activate");
					waiting = false;
				}
			}
		}
		else
		{
			renderer.material.DOColor(Color.black, "Color_54C200CD", transitionTime);
			if (light != null)
			{
				light.enabled = false;
			}
			if (lightShaft != null)
			{
				lightShaft.DOScaleY(yPos[1], transitionTime);
				var emission = GetComponentInChildren<ParticleSystem>().emission;
				emission.enabled = false;
			}
			if (attachedLighting != null)
			{
				//attachedLighting.SendMessage("Activate");
				if (waiting && timer <= 0)
				{
					attachedLighting.SendMessage("Activate");
					waiting = false;
				}
			}
		}
	}
}
