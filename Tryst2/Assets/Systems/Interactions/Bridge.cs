﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Bridge : MonoBehaviour
{
	[SerializeField] private bool isActive = false;
	private bool isLift = false;
	[SerializeField] private float transitionTime = 1f;

	private float targetZ;
	private float unscaledZ = 0.5f;

	private LineRenderer line;

	private Vector3[] linePos = new Vector3[3];
	//0 = start
	//1 = end unscaled
	//2 = end scaled

	private void Start()
	{
		line = GetComponent<LineRenderer>();

		if (!isLift)
		{
			targetZ = transform.localScale.z;
		} else
		{
			targetZ = transform.localScale.y;
		}
		if (isActive)
		{
			if (!isLift)
			{
				transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, targetZ);
				if (line != null)
				{
					line.enabled = true;
				}
			} else
			{
				transform.localScale = new Vector3(transform.localScale.x, targetZ, transform.localScale.z);
				if (line != null)
				{
					line.enabled = true;
				}
			}
		} else
		{
			if (!isLift)
			{
				transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, unscaledZ);
				if (line != null)
				{
					line.enabled = true;
				}
			}
			else
			{
				transform.localScale = new Vector3(transform.localScale.x, unscaledZ, transform.localScale.z);
				if (line != null)
				{
					line.enabled = true;
				}
			}
		}

		if (line != null)
		{
			linePos[0] = transform.position;
			linePos[1] = transform.position;
			linePos[2] = transform.position;

			linePos[1].z -= unscaledZ;
			linePos[2].z -= targetZ;

			line.SetPosition(0, linePos[0]);
		}
	}

	public void Activate()
	{
		if (isActive)
		{
			isActive = false;
		}
		else
		{
			isActive = true;
		}
	}

	void ScaleLineRenderer(int index, Vector3 targetPos)
	{
		Vector3 currentPos = line.GetPosition(index);
		currentPos = Vector3.Lerp(currentPos, targetPos, transitionTime/2);
		line.SetPosition(index, currentPos);
	}

	void Update()
    {
		if (isActive)
		{
			if (!isLift)
			{
				transform.DOScaleZ(targetZ, transitionTime);
				if (line != null)
				{
					line.enabled = true;
					ScaleLineRenderer(1, linePos[2]);
				}
			} else
			{
				transform.DOScaleY(targetZ, transitionTime);
				if (line != null)
				{
					line.enabled = true;
					ScaleLineRenderer(1, linePos[2]);
				}
			}
		} else
		{
			if (!isLift)
			{
				transform.DOScaleZ(unscaledZ, transitionTime);
				if (line != null)
				{
					line.enabled = false;
					ScaleLineRenderer(1, linePos[1]);
				}
			}
			else
			{
				transform.DOScaleY(unscaledZ, transitionTime);
				if (line != null)
				{
					line.enabled = false;
					ScaleLineRenderer(1, linePos[1]);
				}
			}
		}
    }
}
