﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleActivator : MonoBehaviour
{
	private Light lightSource;
	private ParticleSystem particles;
	private bool isActive;

	public void Start()
	{
		lightSource = GetComponent<Light>();
		particles = GetComponent<ParticleSystem>();
	}

	public void Activate()
	{
		if (isActive)
		{
			isActive = false;
			if(particles != null)
			{
				var emission = particles.emission;
					emission.enabled = false;
			}
		} else
		{
			isActive = true;
			if (particles != null)
			{
				var emission = particles.emission;
				emission.enabled = true;
			}
		}
	}
}
