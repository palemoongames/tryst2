﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LightHoleActivator : MonoBehaviour
{
	private bool hasBeenUsed;
	private GameObject cube;
	[SerializeField] private Transform point;
	private Vector3 targetPos;
	//private Vector3 cubeOffset = new Vector3(-0.5f,0f,0.5f);
	private Vector3 cubeOffset = new Vector3(0f, 0.5f, 0f);
	private float timeToPos = 0.1f;

	private void Start()
	{
		targetPos = transform.position + cubeOffset;//point.position;
		DOTween.Init();
	}

	void Update()
    {
		if (hasBeenUsed)
		{
			return;
		}

		RaycastHit[] hits = Physics.SphereCastAll(transform.position, 1.5f, Vector3.up);
		foreach (RaycastHit hit in hits)
		{
			if (hit.collider.tag == "Holdable")
			{
				if (hit.collider.GetComponent<MaterialLighting>().isActive && !hit.collider.GetComponent<Cube>().isHeld)
				{
					cube = hit.collider.gameObject;
					
					cube.transform.eulerAngles = Vector3.zero;
				}
			}
		}

		if (cube == null)
		{
			return;
		}

		if (cube.GetComponent<Cube>().isHeld)
		{
			cube = null;
			return;
		}
		/*if(DOTweenModulePhysics.DOMove(cube.GetComponent<Rigidbody>(), targetPos, timeToPos).playedOnce)
		{
			print("3");
			hasBeenUsed = true;
		}*/
		cube.transform.position = Vector3.Lerp(cube.transform.position, targetPos, timeToPos);
		if(Vector3.Distance(cube.transform.position, targetPos) < 0.05f)
		{
			cube.GetComponent<Cube>().isHeld = true;
			cube.GetComponent<CharacterController>().enabled = false;
			cube.transform.position = targetPos;
			cube.transform.eulerAngles = Vector3.zero;
			cube.tag = "Untagged";
			hasBeenUsed = true;
			GetComponent<BoxCollider>().isTrigger = true;
			SendMessageUpwards("Activate");
		}
	}
}
