﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityWellTrigger : MonoBehaviour
{
	[SerializeField] private GravityWell gwellScript;

	void OnTriggerStay(Collider hit)
	{
		if (hit.gameObject.tag == "Holdable")
		{
			gwellScript.Lift(hit.gameObject);
			hit.gameObject.GetComponent<Cube>().isLifted = true;
		}
	}
	void OnTriggerExit(Collider hit)
	{
		if (hit.gameObject.tag == "Holdable")
		{
			gwellScript.Drop(hit.gameObject);
			hit.gameObject.GetComponent<Cube>().isLifted = false;
		}
	}
}
