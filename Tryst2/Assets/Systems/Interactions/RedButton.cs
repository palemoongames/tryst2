﻿using UnityEngine;
using DG.Tweening;

public class RedButton : MonoBehaviour
{
	[SerializeField] private Transform buttonModel;
	[SerializeField] private GameObject[] objectsToActivate;
	[SerializeField] private Vector3 colliderSize = new Vector3(.5f, .5f, .5f);
	private bool isPressed = false;

	private void Start()
	{
		DOTween.Init();
	}

	private void FixedUpdate()
	{
		CheckForCollisions();
	}
	
	private void CheckForCollisions()
	{
		RaycastHit[] hits = Physics.BoxCastAll(transform.position, colliderSize, Vector3.up,Quaternion.identity, 1f);
		int numOfHits = 0;
		foreach (RaycastHit hit in hits)
		{
			if(hit.collider.tag == "Player1" || hit.collider.tag == "Player2" || hit.collider.tag == "Holdable")
			{
				numOfHits++;
			}
		}
		if (numOfHits > 0)
		{
			ButtonPressed();
		}
		else
		{
			ButtonReleased();
		}
		/*
		isPressed = Physics.BoxCast(transform.position, new Vector3(.5f, .5f, .5f), Vector3.up);
		print(isPressed);
		*/
	}

	void OnTriggerExit(Collider hit)
	{
		//ButtonReleased();
	}

	void OnTriggerStay(Collider hit)
	{
		//ButtonPressed();
	}

		private void ButtonPressed()
	{
		if (!isPressed)
		{
			SendMessageToObjects();
			buttonModel.DOScaleY(0.25f, 0.1f);
		}
		isPressed = true;
	}

	private void ButtonReleased()
	{
		if (isPressed)
		{
			SendMessageToObjects();
			buttonModel.DOScaleY(1, 0.1f);
		}
		isPressed = false;
	}

	private void SendMessageToObjects ()
	{
		foreach (GameObject gameObject in objectsToActivate)
		{
			gameObject.SendMessage("Activate");
		}
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireCube(transform.position, colliderSize * 2);
	}
}