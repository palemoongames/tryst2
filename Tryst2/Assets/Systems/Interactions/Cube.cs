﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
	public bool isHeld;
	public bool isLifted;
	public Controller playerC;
	private Vector3 fallPos;
	private Vector3 lastPos;
	private float modifier = 2f;
	private float moveSpeed = 30f;
	[SerializeField] private float magnitude = 0.1f;

	private void Start()
	{
		lastPos = transform.position;
		lastPos.y += 10f;
	}
	private void Update()
	{
		RaycastHit hit = new RaycastHit();
		if(Physics.Raycast(transform.position, Vector3.down, out hit, Mathf.Infinity))
		{
			if (hit.collider.tag != "Platform" && hit.collider.tag != "Holdable" && !isLifted)
			{
				lastPos = hit.point;
				lastPos.y += 10f;
			}
		} else
		{
			if (!isHeld)
			{
				//GetComponent<Rigidbody>().isKinematic = true;
			}
		}
		if(transform.position.y <= -50)
		{
			fallPos = transform.position;
			ResetCube();
		}
	}

	public void MoveTowardsTarget(Vector3 target)
	{
		CharacterController cc = GetComponent<CharacterController>();
		Vector3 offset = target - transform.position;

		if (offset.magnitude > magnitude)
		{
			//If we're further away than .1 unit, move towards the target.
			//The minimum allowable tolerance varies with the speed of the object and the framerate. 
			// 2 * tolerance must be >= moveSpeed / framerate or the object will jump right over the stop.
			offset = offset.normalized * moveSpeed;
			//normalize it and account for movement speed.
			cc.Move(offset * Time.deltaTime);
			//actually move the character.
		}
	}

	private void ResetCube()
	{
		RaycastHit hit = new RaycastHit();
		if (Physics.Raycast(lastPos, Vector3.down, out hit, Mathf.Infinity))
		{
			lastPos = hit.point;
			lastPos.y += 1f;
			fallPos.y = lastPos.y;
			fallPos = (fallPos - lastPos).normalized;
			lastPos -= fallPos;
		}
		GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
		GetComponent<Rigidbody>().velocity = Vector3.zero;
		transform.eulerAngles = Vector3.zero;
		transform.position = lastPos;
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Well")
		{

		}
	}
}
