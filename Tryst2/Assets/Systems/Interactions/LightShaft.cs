﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightShaft : MonoBehaviour
{
	private void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Holdable")
		{
			other.SendMessage("Activate");
		}
	}
}
