﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum UIAnimationTypes
{
	Move,
	Scale,
	ScaleX,
	ScaleY,
	Fade
}

public class UITweener : MonoBehaviour
{
	public GameObject objectToAnimate;

	public UIAnimationTypes anmimationType;
	public Ease easeType;
	public float duration;
	public float delay;

	public int loops;
	public LoopType loopType;

	public bool startPositionOffset;
	public Vector3 from;
	public Vector3 to;

	private Tween tweenObject;

	public bool showOnEnable;
	public bool workOnDisable;

	public void OnEnable()
	{
		if (showOnEnable)
		{
			Show();
		}
	}

	public void Show()
	{
		HandleTween();
	}

	public void HandleTween()
	{
		if (objectToAnimate == null)
		{
			objectToAnimate = gameObject;
		}

		switch (anmimationType)
		{
			case UIAnimationTypes.Fade:
				Fade();
				break;
			case UIAnimationTypes.Move:
				Move();
				break;
			case UIAnimationTypes.Scale:
				Scale();
				break;
			case UIAnimationTypes.ScaleX:
				Scale();
				break;
			case UIAnimationTypes.ScaleY:
				Scale();
				break;
		}

		tweenObject.SetDelay(delay);
		tweenObject.SetEase(easeType);

		if (loops != 0)
		{
			tweenObject.SetLoops(loops, loopType);
		}
	}

	public void Move()
	{
		objectToAnimate.GetComponent<RectTransform>().anchoredPosition = from;

		tweenObject = objectToAnimate.GetComponent<RectTransform>().DOLocalMove(to, duration);
	}

	public void Scale()
	{
		if (startPositionOffset)
		{
			objectToAnimate.GetComponent<RectTransform>().localScale = from;
		}
		tweenObject = objectToAnimate.GetComponent<RectTransform>().DOScale(to, duration);
	}

	public void Fade()
	{
		if (gameObject.GetComponent<CanvasGroup>() == null) {
			gameObject.AddComponent<CanvasGroup>();
		}

		if (startPositionOffset)
		{
			objectToAnimate.GetComponent<CanvasGroup>().alpha = from.x;
		}
		tweenObject = objectToAnimate.GetComponent<CanvasGroup>().DOFade(to.x, duration);
	}

	public void Flip()
	{
		var temp = from;
		from = to;
		to = temp;
	}
}
