﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

public class LevelManager : MonoBehaviour{

	[SerializeField] private bool isFrontEnd;
	[SerializeField] private Levelator goal;
	[SerializeField] private GameObject[] playerSpawns;
	[SerializeField] private GameObject[] playerCharacters;
	[SerializeField] private Animator transition;
	[SerializeField] private Image progressBar;
	[SerializeField] private GameObject progressObject;
	[SerializeField] private TextMeshProUGUI titleUI;

	private UITweener mainMenuCanvas;

	private bool isLoading;
    public GameObject killZ;
    [HideInInspector]
	
	private void Awake ()
	{
		SaveData.current.Load();

		if(SceneManager.GetActiveScene().buildIndex != 0 && SceneManager.GetActiveScene().buildIndex != 1)
		{
			SaveData.current.lastLevelPlayed = SceneManager.GetActiveScene().buildIndex;

		}
		if (SaveData.current.lastLevelPlayed > SaveData.current.highestUnlockedLevel)
		{
			SaveData.current.highestUnlockedLevel = SaveData.current.lastLevelPlayed;
		}

		Debug.Log(SaveData.current.lastLevelPlayed + "-" + SaveData.current.highestUnlockedLevel);

		SaveData.current.Save();
	}

	private void Start()
	{
		DOTween.SetTweensCapacity(1250, 50);
        titleUI.text = SceneManager.GetActiveScene().name.Replace("_", " ");

		if (playerSpawns.Length > 1 && !isFrontEnd)
		{
			for (int i = 0; i < playerCharacters.Length; i++)
			{
				playerCharacters[i] = Instantiate(playerCharacters[i], playerSpawns[i].transform.position, playerSpawns[i].transform.rotation);
				playerCharacters[i].name = "Player" + (i + 1);
				Destroy(playerSpawns[i]);
			}
		}
		if (isFrontEnd)
		{
			mainMenuCanvas = GameObject.Find("MainMenuCanvas").GetComponentInChildren<UITweener>();
		}
	}

	private void Update()
	{
		if (isFrontEnd)
		{
			return;
		}
		if (goal != null) 
		{
			if (goal.isComplete && !isLoading)
			{
				isLoading = true;
				LoadNextLevel();
			}
		}

		if (Input.GetKeyDown(KeyCode.P))
		{
			SaveData.current.Reset();
		}

		if (Input.GetButtonDown("ResetLevel"))
		{
			StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex));
		}
	}

	public void LoadNextLevel()
	{
		foreach (GameObject player in playerCharacters)
		{
			player.GetComponent<Controller>().LevelCompleted();
		}

		int levelIndex = SceneManager.GetActiveScene().buildIndex;
		if (levelIndex <= SceneManager.sceneCount + 1)
		{
			levelIndex++;
		} else
		{
			levelIndex = 0;
		}

		StartCoroutine(LoadLevel(levelIndex));
	}

	public void LoadLastLevelPlayed()
	{
		StartCoroutine(LoadLevel(SaveData.current.lastLevelPlayed));
	}

	public void SetLevel(int levelIndex)
	{
		StartCoroutine(LoadLevel(levelIndex));

		if (mainMenuCanvas)
		{
			mainMenuCanvas.delay = 0;;
			mainMenuCanvas.Flip();
			mainMenuCanvas.Show();
		}
	}

	IEnumerator LoadLevel (int levelIndex)
	{
		transition.SetTrigger("Start");
		//extra transition time for end of levels
		if (!isFrontEnd) {
			yield return new WaitForSeconds(4f);
		}
		//2s transition time for black fade
		progressObject.SetActive(true);
		yield return new WaitForSeconds(2f);

		AsyncOperation operation = SceneManager.LoadSceneAsync(levelIndex);

		while (!operation.isDone)
		{
			float progress = Mathf.Clamp01(operation.progress / 0.9f);
			progressBar.fillAmount = progress;
			
			yield return null;
		}
	}
}