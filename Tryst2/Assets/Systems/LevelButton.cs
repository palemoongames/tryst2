﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour{

    public int levelNumber;

    void Start(){
        if (PlayerPrefs.GetInt("highestUnlockedLevel") >= levelNumber +1) {
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(1).GetComponent<Image>().enabled = false;
            GetComponent<Button>().enabled = true;
        } else {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).GetComponent<Image>().enabled = true;
            GetComponent<Button>().enabled = false;
        }

        if (name == "1") {
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(1).GetComponent<Image>().enabled = false;
            GetComponent<Button>().enabled = true;
        }
    }

    public void LoadLevel(){
        SceneManager.LoadScene(levelNumber+1);
    }
}