﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LightObjects))]
public class LightBuilder : Editor
{
	private GameObject currentLightObject;

	private LightObjects lightObjects;

	private float bridgeSize = 0.5f;

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		lightObjects = (LightObjects)target;

		currentLightObject = lightObjects.currentLightObject;

		if (currentLightObject == null)
		{
			if(GUILayout.Button("Start Building"))
			{
				currentLightObject = lightObjects.BuildStart();
			}
			return;
		}	

		if (currentLightObject.name == "Light Hole")
		{
			GUILayout.Label("Rotate Starting Piece");

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Rotate Counter-Clockwise"))
			{
				lightObjects.RotateCounterClockwise(currentLightObject);
			}

			if (GUILayout.Button("Rotate Clockwise"))
			{
				lightObjects.RotateClockwise(currentLightObject);
			}
			GUILayout.EndHorizontal();
		}

		if (currentLightObject.name == "Light End")
		{
			GUILayout.Label("What to activate?");

			GUILayout.BeginHorizontal();

			if (GUILayout.Button("Bridge"))
			{
				currentLightObject = lightObjects.BuildBridge(false);
				bridgeSize = 0.5f;
			}

			GUILayout.EndHorizontal();

			if (GUILayout.Button("Delete"))
			{
				DestroyImmediate(currentLightObject);
				currentLightObject = lightObjects.DeleteObject();
			}
			return;
		}

		if (currentLightObject.name == "Light Bridge")
		{
			bridgeSize = EditorGUILayout.FloatField(bridgeSize);

			GUILayout.Label("How far should the bridge extend?");

			if (GUILayout.Button("UpdateSize"))
			{
				lightObjects.UpdateBridge(bridgeSize);
			}
			return;
		}
		if (currentLightObject.name == "Light Bridge Scaled")
		{
			bridgeSize = EditorGUILayout.FloatField(bridgeSize);

			GUILayout.Label("Resize the bridge");

			if (GUILayout.Button("UpdateSize"))
			{
				lightObjects.UpdateBridge(bridgeSize);
			}

			if (GUILayout.Button("Delete"))
			{
				DestroyImmediate(currentLightObject);
				currentLightObject = lightObjects.DeleteObject();
			}

			return;
		}

		GUILayout.Label("Next Piece");

		GUILayout.BeginHorizontal();

		if (GUILayout.Button("Straight"))
		{
			currentLightObject = lightObjects.BuildStraight();
		}
		if (GUILayout.Button("Left"))
		{
			currentLightObject = lightObjects.BuildCorner(false);
		}
		if (GUILayout.Button("Right"))
		{
			currentLightObject = lightObjects.BuildCorner(true);
		}
		/*if (GUILayout.Button("Beam"))
		{
			currentLightObject = lightObjects.BuildBridge(true);
		}*/
		if (GUILayout.Button("End"))
		{
			currentLightObject = lightObjects.BuildEnd(false);
		}

		GUILayout.EndHorizontal();

		if (GUILayout.Button("Delete"))
		{
			DestroyImmediate(currentLightObject);
			currentLightObject = lightObjects.DeleteObject();
		}
	}
}
