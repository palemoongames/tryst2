﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GravityWell))]
public class GravityWellBuilder : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		GravityWell script = (GravityWell)target;

		if (GUILayout.Button("Build")){

			script.InitGravityWell();
		}
	}
}
