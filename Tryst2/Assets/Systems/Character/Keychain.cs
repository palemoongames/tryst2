﻿using UnityEngine;

public enum KEY_COLOUR
{
	WHITE,//0
	BLUE,//1
	ORANGE,//2
	GREEN,//3
	PINK,//4
}

public class Keychain : MonoBehaviour
{
	public KEY_COLOUR currentKeyColour = KEY_COLOUR.WHITE;
	public Material shader = null;
	public int playerID;
	public bool hasPhysicalKey;

	private void OnTriggerEnter(Collider other)
	{
		if(tag != "Key" && other.tag == "Key" && other && other.GetComponent<Keychain>().playerID != gameObject.GetInstanceID())
		{
			Keychain otherKeychain = other.GetComponent<Keychain>();

			KEY_COLOUR tmpKeyColour = currentKeyColour;
			currentKeyColour = otherKeychain.currentKeyColour;
			otherKeychain.currentKeyColour = tmpKeyColour;
			otherKeychain.playerID = gameObject.GetInstanceID();

			Material tmpMat = shader;
			shader = otherKeychain.shader;
			otherKeychain.shader = tmpMat;

			//GetComponent<Controller>().newColour = shader;
			foreach (Renderer rend in GetComponentsInChildren<Renderer>())
			{
				rend.material = shader;
			}
			other.GetComponentInChildren<Renderer>().material = otherKeychain.shader;

			Light light = GetComponentInChildren<Light>();
			Color tmpColour = light.color;
			light.color = other.GetComponentInChildren<Light>().color;
			other.GetComponentInChildren<Light>().color = tmpColour;

			int tmpLayer = gameObject.layer;
			gameObject.layer = other.gameObject.layer;
			other.gameObject.layer = tmpLayer;
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (tag != "Key" && other.tag == "Key" && other && other.GetComponent<Keychain>().playerID == gameObject.GetInstanceID())
		{
			other.GetComponent<Keychain>().playerID = 0;
		}
	}
}
