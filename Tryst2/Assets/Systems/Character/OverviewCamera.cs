﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverviewCamera : MonoBehaviour
{
	private Vector3 targetPos;
	private GameObject playerOne, playerTwo, worldAxis;

	private void Start()
	{
		playerOne = GameObject.FindWithTag("Player1");
		playerTwo = GameObject.FindWithTag("Player2");
		worldAxis = GameObject.FindWithTag("Axis");
	}

	private void FixedUpdate()
    {
		if (playerOne != null && playerTwo != null)
		{
			targetPos = (playerOne.transform.position + playerTwo.transform.position) / 2;
			targetPos.y = 0;
			worldAxis.transform.position = targetPos;
		} else
		{
			playerOne = GameObject.FindWithTag("Player1");
			playerTwo = GameObject.FindWithTag("Player2");
		}
    }
}
