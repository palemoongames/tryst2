﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
	private GameObject playerOne, playerTwo;

	[SerializeField] private GameObject[] cineCams;
	private int currentCam;
    float camDist;
    
    private void Update()
    {
		if (playerOne == null || playerTwo == null)
		{
			playerOne = GameObject.FindWithTag("Player1");
			playerTwo = GameObject.FindWithTag("Player2");
			return;
		}
		cineCams[1].GetComponent<CinemachineVirtualCamera>().Follow = playerOne.transform;
		cineCams[2].GetComponent<CinemachineVirtualCamera>().Follow = playerTwo.transform;

		if (Input.GetKeyDown(KeyCode.Q) || Input.GetButtonDown("CamSwitch"))
		{
			NextCam();
		}

        if(currentCam == 0) {
            camDist = Mathf.Clamp(Vector2.Distance(GameObject.FindGameObjectWithTag("Player1").transform.position, GameObject.FindGameObjectWithTag("Player2").transform.position), 15, 30);
            Camera.main.fieldOfView = camDist;
        }
    }

	private void NextCam()
	{
		if(currentCam < cineCams.Length -1)
		{
			currentCam++;
			cineCams[currentCam].SetActive(true);
			cineCams[currentCam - 1].SetActive(false);
		} else
		{
			currentCam = 0;
			cineCams[currentCam].SetActive(true);
			cineCams[cineCams.Length - 1].SetActive(false);
		}
		
	}
}
