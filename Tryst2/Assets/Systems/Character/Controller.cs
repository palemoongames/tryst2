﻿using UnityEngine;

public class Controller : MonoBehaviour {

    private const float GRAVITY = 9.8f;
    private string HORIZONTAL = "Horizontal";
    private string VERTICAL = "Vertical";
    private string GRAB = "P1Grab";
	private GameObject holdableObject;
	public GameObject heldObject = null;
    private Quaternion rotation = Quaternion.identity;
    private bool isFalling = false;
    private float fallSpeed = 0f;
	private bool doFallChecks = false;
	private Renderer[] renderers;
	private Light playerLight;
	private bool hasSpawned;
	private bool hasFinished;
	private float startTime;
	private float disolveSpeed = 0.5f;
	public Material newColour;
	[SerializeField] private Transform holdableOffset = null;
    [SerializeField] private CharacterController controller = null;
    [SerializeField] private Transform worldAxis = null;
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float turnSpeed = 20f;
    [SerializeField] private Transform playerModel = null;


    void Start() {
        if(CompareTag("Player2")) {
            HORIZONTAL = "Horizontal2";
            VERTICAL = "Vertical2";
            GRAB = "P2Grab";
        }
		worldAxis = GameObject.FindWithTag("Axis").transform;

		renderers = GetComponentsInChildren<Renderer>();
		playerLight = GetComponentInChildren<Light>();
		startTime = Time.time;
		foreach (Renderer rend in renderers)
		{
			//this channel value is backwards (:
			rend.material.SetFloat("_Alpha", 1);
		}
		playerLight.intensity = 0;

		newColour = GetComponentInChildren<Renderer>().material;
    }

	void FixedUpdate() {
		if (!hasSpawned)
		{
			DisolveCharacter(1, 0, disolveSpeed);
			LightCharacter(0, 0.1f, disolveSpeed);
		}
		if (hasFinished)
		{
			DisolveCharacter(0, 1, disolveSpeed);
			LightCharacter(0.1f, 0, disolveSpeed);
		}

		Vector3 moveXY;
		moveXY = new Vector3(Input.GetAxisRaw(HORIZONTAL), 0, Input.GetAxisRaw(VERTICAL));
		moveXY = moveXY.normalized;
		Vector3 direction = worldAxis.TransformDirection(moveXY);
		Vector3 forwardGroundCheck = transform.position + direction / 3;
		forwardGroundCheck.y += 1f;

		if (doFallChecks)
		{
			if (CheckForward(direction))
			{
				Move(direction * moveSpeed * Time.deltaTime);
			}
		} else
		{
			Move(direction * moveSpeed * Time.deltaTime);
		}
		
		Vector3 desiredForward = Vector3.RotateTowards(playerModel.forward, worldAxis.TransformDirection(moveXY), turnSpeed * Time.deltaTime, 0f);
        rotation = Quaternion.LookRotation(desiredForward);
        //playerModel.rotation = (rotation);

        //GRAB OBJECT
        if(Input.GetButton(GRAB)) {
            if(holdableObject != null && heldObject == null) {
                heldObject = holdableObject;

                Rigidbody cubeBody = heldObject.GetComponent<Rigidbody>();
				cubeBody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
				cubeBody.useGravity = false;
				cubeBody.isKinematic = true;

				heldObject.transform.eulerAngles = playerModel.eulerAngles;

				Cube cubeScript = heldObject.GetComponent<Cube>();
				cubeScript.isHeld = true;
				cubeScript.playerC = this;
			}
        }
        //DROP OBJECT
        else {
            if(heldObject != null) {
				DropCube();
			}
        }
		if (heldObject)
		{
			RaycastHit hit = new RaycastHit();
			Vector3 rayDir = (heldObject.transform.position - playerModel.position).normalized;
			Debug.DrawRay(playerModel.position, rayDir, Color.blue);
			if (Physics.Raycast(playerModel.position, rayDir, out hit, 5f))
			{
				Debug.DrawLine(playerModel.position, hit.point, Color.red);
				if (hit.collider.gameObject.GetInstanceID() != heldObject.GetInstanceID())
				{
					DropCube();
					return;
				}
			}
			Vector3 target = transform.TransformPoint(playerModel.forward * 1.5f);
			heldObject.GetComponent<Cube>().MoveTowardsTarget(target);

			heldObject.transform.eulerAngles = playerModel.eulerAngles;

			
		}
	}

	private void DropCube()
	{
		Rigidbody cubeBody = heldObject.GetComponent<Rigidbody>();
		cubeBody.constraints = RigidbodyConstraints.None;
		cubeBody.useGravity = true;
		cubeBody.isKinematic = false;

		heldObject.transform.eulerAngles = playerModel.eulerAngles;

		Cube cubeScript = heldObject.GetComponent<Cube>();
		cubeScript.isHeld = false;
		cubeScript.playerC = null;
		heldObject = null;
	}

    private void LateUpdate() {
		float lastYPos = transform.position.y;
		Vector3 velocity = new Vector3(0, -fallSpeed, 0);
		Move(velocity * Time.deltaTime);

		if (lastYPos > transform.position.y)
		{
			isFalling = true;
		}
		else
		{
			isFalling = false;
		}

		if (isFalling)
		{
			fallSpeed += GRAVITY * Time.deltaTime;
		}
		else
		{
			fallSpeed = GRAVITY;
		}
	}

	private void Move (Vector3 moveVector)
	{
		controller.Move(moveVector);
	}

	private bool CheckForward(Vector3 dir)
	{
		bool hitCheck = false;

		Vector3 offset = transform.position;
		offset.y += 1f;
		Vector3 forwardGroundCheck = offset + dir / 3;

		RaycastHit hit = new RaycastHit();

		if (Physics.Raycast(forwardGroundCheck, Vector3.down, out hit, 10f))
		{
			hitCheck = true;
			if (hit.collider.tag == "Holdable")
			{
				hitCheck = false;
			}			
		}
		if (Physics.Raycast(transform.position, Vector3.down, out hit, 1f))
		{
			if (hit.collider.tag == "Platform")
			{
				controller.Move(hit.collider.GetComponent<Platform>().externalMoveSpeed);
			}
		}
		return hitCheck;
	}

	public void DisolveCharacter(float start, float end, float speed)
	{
		float distCovered = (Time.time - startTime) * speed;
		float fractionOfDisolve = distCovered / 1;

		foreach (Renderer rend in renderers)
		{
			rend.material.SetFloat("_Alpha", Mathf.Lerp(start, end, fractionOfDisolve));
		}
		if (GetComponentInChildren<Renderer>().material.GetFloat("_Alpha") == end && newColour != GetComponentInChildren<Renderer>().material)
		{
			SetColours();
		}
	}

	public void LightCharacter(float start, float end, float speed)
	{
		float distCovered = (Time.time - startTime) * speed;
		float fractionOfDisolve = distCovered / 1;

		playerLight.intensity = Mathf.Lerp(start, end, fractionOfDisolve);
		if (playerLight.intensity == end)
		{
			hasSpawned = true;
		}
	}

	public void SetColours()
	{
		foreach (Renderer rend in GetComponentsInChildren<Renderer>())
		{
			rend.material = newColour;
		}
	}

	public void LevelCompleted()
	{
		hasFinished = true;
		startTime = Time.time;
	}

	void OnTriggerStay(Collider hit) {
		if (hit.tag == "Holdable")
		{
			if (!hit.GetComponent<Cube>().isHeld) { 
				holdableObject = hit.gameObject;
			}
        }
    }

    void OnTriggerExit(Collider hit) {
        if(hit.tag == "Holdable") {
            holdableObject = null;
        }
    }
}