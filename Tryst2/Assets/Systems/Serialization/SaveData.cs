﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SaveData{

	private static SaveData _current;
	public static SaveData current{
		get{
			if(_current == null){
				_current = new SaveData();
			}
			return _current;
		}
	}

	public int highestUnlockedLevel;
	public int lastLevelPlayed;
	public int gameIsComplete;

	public void Save(){
		PlayerPrefs.SetInt("highestUnlockedLevel", highestUnlockedLevel);
		PlayerPrefs.SetInt("lastLevelPlayed", lastLevelPlayed);
		PlayerPrefs.SetInt("gameIsComplete", gameIsComplete);
	}

	public void Load(){
		highestUnlockedLevel = PlayerPrefs.GetInt("highestUnlockedLevel");
		lastLevelPlayed = PlayerPrefs.GetInt("lastLevelPlayed");
		gameIsComplete = PlayerPrefs.GetInt("gameIsComplete");
	}

	public void Reset(){
		PlayerPrefs.DeleteAll();
	}
}