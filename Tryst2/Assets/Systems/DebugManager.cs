﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugManager : MonoBehaviour{

	public bool isDebug;

	private void Awake(){
		if (!isDebug){
			GameObject[] debugObjects = GameObject.FindGameObjectsWithTag("Debug");
			foreach (GameObject dObject in debugObjects){
				dObject.GetComponent<MeshRenderer>().enabled = false;
			}

            if (GameObject.Find("Dev Notes")) {
                GameObject.Find("Dev Notes").SetActive(false);
            }
		}
	}
}