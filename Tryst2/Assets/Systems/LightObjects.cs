﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightObjects : MonoBehaviour
{
	public GameObject[] lightPrefabs;

	private List<GameObject> builtObjects;
	private List<Vector3> directions;

	public GameObject currentLightObject;

	private int totalObjects;
	private Transform currentTransform;
	private float prefabSize;

	public GameObject BuildStart()
	{
		Vector3 absPos = transform.position;
		absPos.x = Mathf.Round(absPos.x * 2) / 2;
		absPos.y = Mathf.Round(absPos.y * 2) / 2;
		absPos.z = Mathf.Round(absPos.z * 2) / 2;
		transform.position = absPos;

		builtObjects = new List<GameObject>();
		directions = new List<Vector3>();

		currentLightObject = Instantiate(lightPrefabs[0], transform.position, Quaternion.identity);
		currentLightObject.name = "Light Hole";
		currentLightObject.transform.parent = transform;

		currentTransform = currentLightObject.transform;
		totalObjects++;

		prefabSize = currentLightObject.GetComponent<BoxCollider>().size.z;

		builtObjects.Add(currentLightObject);
		directions.Add(currentTransform.forward);

		return currentLightObject;
	}

	public GameObject DeleteObject()
	{
		builtObjects.RemoveAt(builtObjects.Count - 1);
		directions.RemoveAt(directions.Count - 1);
		
		if(builtObjects.Count > 0)
		{
			currentLightObject = builtObjects[builtObjects.Count - 1];
			currentTransform = currentLightObject.transform;
			totalObjects--;
			return currentLightObject;
		}
		currentLightObject = null;
		currentTransform = null;
		return null;
	}

	public Vector3 NextSpawnPos()
	{
		Vector3 temp = currentTransform.position;
		temp += directions[directions.Count-1]* prefabSize;

		return temp;
	}

	public GameObject BuildStraight()
	{
		currentLightObject = Instantiate(lightPrefabs[1], NextSpawnPos(), currentTransform.rotation);
		currentLightObject.name = "Light Stright";

		currentLightObject.transform.parent = transform;

		currentTransform = currentLightObject.transform;

		Quaternion lookAt = Quaternion.LookRotation(NextSpawnPos() - currentTransform.position);
		currentTransform.rotation = Quaternion.Lerp(transform.rotation, lookAt, 1);
		SetAbsRotation();
		SetAbsPosition();

		totalObjects++;

		builtObjects.Add(currentLightObject);
		directions.Add(currentTransform.forward);

		prefabSize = currentLightObject.GetComponent<BoxCollider>().size.z;

		HookUpScript();

		return currentLightObject;
	}

	public GameObject BuildCorner(bool isLeft)
	{
		currentLightObject = Instantiate(lightPrefabs[2], NextSpawnPos(), currentTransform.rotation);
		currentLightObject.name = "Light Left";
		currentLightObject.transform.parent = transform;

		currentTransform = currentLightObject.transform;
		totalObjects++;

		Quaternion lookAt = Quaternion.LookRotation(NextSpawnPos() - currentTransform.position);
		currentTransform.rotation = Quaternion.Lerp(transform.rotation, lookAt, 1);
		SetAbsRotation();
		SetAbsPosition();

		totalObjects++;

		if (!isLeft)
		{
			currentLightObject.name = "Light Right";

			Vector3 scale = currentTransform.localScale;
			scale.x *= -1;

			currentTransform.localScale = scale;
			directions.Add(-currentTransform.right);
		} else
		{
			directions.Add(currentTransform.right);
		}

		builtObjects.Add(currentLightObject);

		prefabSize = currentLightObject.GetComponent<BoxCollider>().size.z;

		HookUpScript();

		return currentLightObject;
	}

	public GameObject BuildEnd(bool isPastBridge)
	{
		currentLightObject = Instantiate(lightPrefabs[3], NextSpawnPos(), currentTransform.rotation);
		currentLightObject.name = "Light End";
		currentLightObject.transform.parent = transform;

		currentTransform = currentLightObject.transform;

		totalObjects++;

		Quaternion lookAt = Quaternion.LookRotation(NextSpawnPos() - currentTransform.position);
		currentTransform.rotation = Quaternion.Lerp(transform.rotation, lookAt, 1);
		SetAbsRotation();
		SetAbsPosition();

		totalObjects++;

		builtObjects.Add(currentLightObject);
		directions.Add(currentTransform.forward);

		prefabSize = currentLightObject.GetComponent<BoxCollider>().size.z;

		if (isPastBridge)
		{
			currentLightObject.name = "Light Continued";
			currentLightObject.transform.localEulerAngles += new Vector3(0, 180, 0);
		}

		HookUpScript();

		return currentLightObject;
	}

	public GameObject BuildBridge(bool isBeam)
	{
		Vector3 newSpawnPos = currentTransform.position;
		newSpawnPos += directions[directions.Count - 1] * prefabSize/2;

		if (isBeam)
		{
			currentLightObject = Instantiate(lightPrefabs[5], newSpawnPos, currentTransform.rotation);
			currentLightObject.name = "Light Beam";
		}
		else
		{
			currentLightObject = Instantiate(lightPrefabs[4], newSpawnPos, currentTransform.rotation);
			currentLightObject.name = "Light Bridge";
		}		
		currentLightObject.transform.parent = transform;

		currentTransform = currentLightObject.transform;

		totalObjects++;

		Quaternion lookAt = Quaternion.LookRotation(NextSpawnPos() - currentTransform.position);
		currentTransform.rotation = Quaternion.Lerp(transform.rotation, lookAt, 1);
		SetAbsRotation();
		SetAbsPosition();

		totalObjects++;

		builtObjects.Add(currentLightObject);
		directions.Add(currentTransform.forward);

		prefabSize = currentLightObject.GetComponent<MeshRenderer>().bounds.size.z;

		HookUpScript();

		return currentLightObject;
	}

	public void UpdateBridge(float bridgeSize)
	{
		Vector3 newBridgeScale = new Vector3(1, 1, bridgeSize);
		currentTransform.localScale = newBridgeScale;

		currentLightObject.name = "Light Bridge Scaled";

		prefabSize -= currentLightObject.GetComponent<BoxCollider>().size.z;
	}

	public void HookUpScript()
	{
		MaterialLighting tempScript = builtObjects[builtObjects.Count - 2].GetComponent<MaterialLighting>();
		tempScript.attachedLighting = currentLightObject;
	}

	public void RotateClockwise(GameObject currentObject)
	{
		currentObject.transform.localEulerAngles += new Vector3(0, 90, 0);
		SetAbsRotation();
		directions.Clear();
		directions.Add(currentTransform.forward);
	}

	public void RotateCounterClockwise(GameObject currentObject)
	{
		currentObject.transform.localEulerAngles += new Vector3(0, -90, 0);
		SetAbsRotation();
		directions.Clear();
		directions.Add(currentTransform.forward);
	}

	public void SetAbsRotation()
	{
		Vector3 newEulerAngles = currentTransform.eulerAngles;
		newEulerAngles.y = Mathf.RoundToInt(newEulerAngles.y);
		currentTransform.eulerAngles = newEulerAngles;
	}
	public void SetAbsPosition()
	{
		Vector3 absPos = currentTransform.position;
		absPos.x = Mathf.Round(absPos.x * 2) / 2;
		absPos.y = Mathf.Round(absPos.y * 2) / 2;
		absPos.z = Mathf.Round(absPos.z * 2) / 2;
		currentTransform.position = absPos;
	}
}
